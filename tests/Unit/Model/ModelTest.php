<?php

namespace App\Tests\Unit\Model;

use App\TestCodecept\Model\Model;
use Codeception\Test\Unit;

class ModelTest extends Unit
{
    public function testRun(): void
    {
        $model = new Model();
        $this->assertTrue($model->run());
    }
}