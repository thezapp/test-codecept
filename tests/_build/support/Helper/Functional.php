<?php

declare(strict_types=1);

namespace App\Tests\Helper;

use App\Entity\Lead;
use Codeception\Module;
use Codeception\Module\Symfony;
use Codeception\Stub;
use Skyeng\IdBundle\Model\User;
use Skyeng\IdClient\DAO\API\SessionDAO as ApiSessionDAO;
use Skyeng\IdClient\DAO\UserAPI\SessionDAO as UserApiSessionDAO;
use Skyeng\IdClient\Model\UserAPI\Session;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class Functional extends Module
{
    private const GLOBAL_SESSION_COOKIE_NAME = 'session_global';
    private const SESSION_LEAD_UUID_COOKIE_NAME = 'teachers-hiring-multiproduct-lead-uuid';
    private const ONBOARDING_LEAD_UUID_COOKIE_NAME = 'teachers-onboarding-lead-id';

    /**
     * Имитирует авторизацию пользователя через ID сервис.
     */
    public function amSkyengUserAuthenticated(int $id, array $roles = []): void
    {
        /** @var Symfony $symfony */
        $symfony = $this->getModule('Symfony');

        /** @var ContainerInterface $container */
        $container = $symfony->grabService('test.service_container');
        /** @var Session $session */
        $session = Stub::make(Session::class, [
            'getId' => $id,
            'getRoles' => $roles,
            'isTwoFactorAuthenticationEnabled' => false,
            'contacts' => [],
        ]);
        $user = Stub::make(User::class, ['getId' => $id, 'getRoles' => $roles]);

        // Для нового апи ИД сервиса. Используется в нашей кастомной авторизации
        $container->set('skyeng_id.dao.user_api.session', Stub::make(UserApiSessionDAO::class, [
            'getSession' => $session,
        ]));

        // Для легаси апи ИД сервиса. Используется в ИД бандле
        $container->set('skyeng_id.dao.api.session', Stub::make(ApiSessionDAO::class, [
            'getSession' => $session,
        ]));

        $cookie = new Cookie(self::GLOBAL_SESSION_COOKIE_NAME, 'skyeng_session_id_goes_here');
        $symfony->client->getCookieJar()->set($cookie);

        /** @var SessionInterface $session */
        $session = $symfony->grabService('session');

        $token = new UsernamePasswordToken($user, null, 'main', $roles);
        $session->set('_security_' . 'main', serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $symfony->client->getCookieJar()->set($cookie);
    }

    /**
     * Имитирует авторизацию лида через куки.
     */
    public function amLeadAuthenticated(Lead $lead): void
    {
        /** @var Symfony $symfony */
        $symfony = $this->getModule('Symfony');
        $cookie = new Cookie(self::SESSION_LEAD_UUID_COOKIE_NAME, $lead->getUuid()->toString());
        $symfony->client->getCookieJar()->set($cookie);
    }

    public function amOnboardingLeadAuthenticated(Lead $lead): void
    {
        /** @var Symfony $symfony */
        $symfony = $this->getModule('Symfony');
        $cookie = new Cookie(self::ONBOARDING_LEAD_UUID_COOKIE_NAME, $lead->getUuid()->toString());
        $symfony->client->getCookieJar()->set($cookie);
    }

    public function removeFromRepository(object $entity): void
    {
        /** @var Symfony $symfony */
        $symfony = $this->getModule('Symfony');
        $symfony->_getEntityManager()->remove($entity);
    }
}
